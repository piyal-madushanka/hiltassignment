package com.madusanka.hilttest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import au.eleganemedia.hilltdemoone.databinding.RowHotelBinding
import com.madusanka.hilttest.models.Data
import coil.load

class HotelsAdapter : RecyclerView.Adapter<HotelsAdapter.HotelsViewHolder>() {
    inner class HotelsViewHolder(
        val binding: RowHotelBinding
    ) :
        RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun submitList(list: List<Data>) = differ.submitList(list)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotelsViewHolder {
        return HotelsViewHolder(
            RowHotelBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: HotelsViewHolder, position: Int) {
        val currentHotel = differ.currentList[position]
        holder.binding.apply {
            textViewHotel.text = currentHotel.title
            val imageLink = currentHotel.image.large
            imageViewHotel.load(imageLink) {
                crossfade(true)
                crossfade(1000)
            }
            textViewAddress.text = currentHotel.address


        }
    }

    override fun getItemCount() = differ.currentList.size
}
