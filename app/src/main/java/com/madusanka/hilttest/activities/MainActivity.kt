package com.madusanka.hilttest.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.madusanka.hilttest.adapters.HotelsAdapter
import au.eleganemedia.hilltdemoone.databinding.ActivityMainBinding
import com.madusanka.hilttest.viewmodels.HotelsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var hotelsAdapter: HotelsAdapter
    private val viewModel: HotelsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpRecycleView()
    }

    private fun setUpRecycleView() {
        hotelsAdapter = HotelsAdapter()
        binding.recycleViewHotels.apply {
            adapter = hotelsAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
        }
        viewModel.responseHotels.observe(this, { response ->
            hotelsAdapter.submitList(response.data)
        })
    }
}