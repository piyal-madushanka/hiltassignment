package com.madusanka.hilttest.repositories

import com.madusanka.hilttest.apis.HotelsService
import javax.inject.Inject

class HotelsRepository
@Inject constructor(private val api: HotelsService) {
    suspend fun getAllHotels() = api.getAllHotels()
}
